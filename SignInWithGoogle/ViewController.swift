//
//  ViewController.swift
//  SignInWithGoogle
//
//  Created by Juri Jeong on 9/11/2022.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController {
    
    @IBOutlet weak var email: UILabel!
    
    let signInConfig = GIDConfiguration(clientID: "1011356824383-cgr3s470t3l9od3ibs4qf7jg424psnu7.apps.googleusercontent.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
    }
    
    private func initViews() {
        let signInButton = GIDSignInButton(frame: CGRect(x: 0,
                                                                y: 0,
                                                                width: 100,
                                                                height: 50))
        signInButton.center = view.center
        view.addSubview(signInButton)
    }
    
    @IBAction func googleSignIn(_ sender: Any) {
        
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            guard error == nil else { return }
            guard
                let user = user,
                let emailAddress = user.profile?.email
            else { return }
        
            self.email.text = emailAddress
            print("email:\((emailAddress))")
        }
    }
    
    @IBAction func googleSignOut(_ sender: Any) {
        GIDSignIn.sharedInstance.signOut()
    }
}

